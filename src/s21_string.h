#ifndef STRING_H
#define STRING_H
#include <stdlib.h>

size_t s21_strlen(const char *str);

int s21_strcmp(char *str1, char *str2);

char *s21_strcpy(char *dest, const char *src);


char *s21_strcat(char *dest, const char *src);

#endif

