#include <stdio.h>
#include "s21_string.h"
#include "s21_string_test.h"
#ifdef QUEST1
#define FUNC() s21_strlen_test()
#elif QUEST2
#define FUNC() s21_strcmp_test()
#elif QUEST3
#define FUNC() s21_strcpy_test()

#endif

int main() {
    printf("\n");
    FUNC();
    return 0;
}

void s21_strlen_test() {
    char *str[] = {"!w/\n.\0", "13\n\03;xc\0", "\0" };
    for (int i = 0; i < 3; i++) {
        printf("%s\n", str[i]);
        printf("%zu\n", s21_strlen(str[i]));
        if ( s21_strlen(str[i]) ) {
            printf("SUCCES\n");
        } else {
            printf("FAIL\n");
        }
    }
}

void s21_strcmp_test() {
    char *str[] = {"12345678\0", "12345688\0", "\0", "2\0", "1\0", "1\0"};
    for (int i = 0; i < 6; i = i + 2) {
        printf("%s\n%s\n", str[i], str[i+1]);
        printf("%d\n", s21_strcmp(str[i], str[i+1]));
        if ( s21_strcmp(str[i], str[i+1]) ) {
        printf("SUCCES\n");
        } else {
        printf("FAIL\n");
        }
    }
}

void s21_strcpy_test() {
    char data1[]={"Welcome\0"};
        char data2[10]={};
        printf("%s\n", data1);
        printf("%s\n", s21_strcpy(data1, data2));
        if ( s21_strcpy(data1, data2) ) {
            printf("SUCCES\n\n");
        } else {
            printf("FAIL\n\n");
        }
        char data3[]={"12345\0"};
        char data4[10]={};
        printf("%s\n", data3);
        printf("%s\n", s21_strcpy(data3, data4));
        if ( s21_strcpy(data3, data4) ) {
            printf("SUCCES\n\n");
        } else {
            printf("FAIL\n\n");
        }
        char data5[]={"123456\0"};
        char data6[10]={};
        printf("%s\n", data5);
        printf("%s\n", s21_strcpy(data5, data6));
        if ( s21_strcpy(data5, data6) ) {
            printf("SUCCES\n\n");
        } else {
            printf("FAIL\n\n");
        }
}
